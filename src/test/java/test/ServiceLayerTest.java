package test;

import static org.junit.Assert.*;


import org.junit.Test;

import dto.ConsultaDTO;
import services.ServiceLayer;

public class ServiceLayerTest {

	@Test
	public void happyPathTest() {
		ConsultaDTO plomeriaSanMiguel = new ConsultaDTO("Plomeria", "San Miguel");
		ServiceLayer service = new ServiceLayer();
		assertTrue(service.find(plomeriaSanMiguel).size() > 0);
	}

}
