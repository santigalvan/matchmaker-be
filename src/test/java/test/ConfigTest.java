package test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import builder.Config;

public class ConfigTest {
	
	Config config;
	String path;
	String file;
	
	@Before
	public void setUp() throws IOException {
		path = System.getProperty("user.dir")+"/resources";
		file = "/sitios.txt";
		config = new Config(path+file);
	}
	
	@Test
	public void happyPathTest() {
		List<String> sitios = new ArrayList<String>();
		sitios.add("builder.MercadoLibreWebBuilder");
		sitios.add("builder.MercadoLibreApiBuilder");
		
		List<String> entries = config.getEntries();
		
		assertArrayEquals(sitios.toArray(), entries.toArray());
	}
	
	@Test
	(expected = IOException.class)
	public void noFileTest() throws IOException {
		config = new Config(path+"/vacio.txt");
	}

}
