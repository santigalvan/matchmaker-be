package test;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

import model.Consulta;
import querier.Querier;
import scrapper.Scrapper;

public class ScrapperTest {
	
	Querier querierMl;
	Scrapper scrapperMl;
	String urlMl;
	Consulta consulta;
	
	@Before
	public void setUp() throws Exception {
		urlMl = "https://servicios.mercadolibre.com.ar";
		querierMl = new Querier(urlMl);
		scrapperMl = new Scrapper(querierMl); 
		consulta = new Consulta("plomero","san miguel");
	}


	@Test
	public void scrapperMlWeb() {
		assertTrue( scrapperMl.find(consulta) != null);
	}
	
	@Test
	public void notEqualsTest() {
		assertTrue(scrapperMl.equals(new Scrapper(new Querier("https://servicios.mercadolibre.kom.ar"))));
	}
	
	@Test
	public void equalsTest() {
		assertTrue(scrapperMl.equals(new Scrapper(new Querier(urlMl))));
	}

}
