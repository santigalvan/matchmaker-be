package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import deserialize.Deserialize;
import model.Consulta;
import model.RespuestaDeserialize;
import querier.Querier;
import scrapper.Scrapper;

public class DeserializeTest {
	
	Querier querierMl;
	Scrapper scrapperMl;
	Deserialize deserializeMl;
	String urlMl;
	Consulta consulta;

	@Before
	public void setUp() throws Exception {
		urlMl = "https://servicios.mercadolibre.com.ar";
		querierMl = new Querier(urlMl);
		scrapperMl = new Scrapper(querierMl);
		deserializeMl = new Deserialize(scrapperMl);
		consulta = new Consulta("plomero","san miguel");
	}

	
	@Test
	public void deserializeMlWebTest() {
		assertTrue(((RespuestaDeserialize)deserializeMl.find(consulta)).getRespuesta().size() > 0);
	}
	
	@Test
	public void notEqualsTest() throws Exception {
		assertTrue(deserializeMl.equals(new Deserialize(new Scrapper(new Querier("https://servicios.mercadolibre.kom.ar")))));
	}
	@Test
	public void EqualsTest() throws Exception {
		assertTrue(deserializeMl.equals(new Deserialize(new Scrapper(new Querier(urlMl)))));
	}

}
