package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import dto.ConsultaDTO;
import dto.DTOMapper;
import model.Consulta;

public class DTOMapperTest {

	

	
	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void dissamblerConsultaDTOTest() {
		ConsultaDTO consultaDTO = new ConsultaDTO("Gasista", "San Miguel");
		Consulta consulta = new Consulta("Gasista", "San Miguel");

		assertEquals(consulta, DTOMapper.dissambler(consultaDTO));
	}

}
