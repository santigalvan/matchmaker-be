package test;

import static org.junit.Assert.*;

import java.io.IOException;
import org.junit.Before;
import org.junit.Test;

import model.Consulta;
import querier.Querier;


public class QuerierTest {
	
	Querier querierMl;
	Querier querierMlUrlInexistente;
	String urlMl;
	Consulta consulta;
	
	@Before
	public void setUp() throws IOException {
		urlMl = "https://servicios.mercadolibre.com.ar";
		querierMl = new Querier(urlMl);
		querierMlUrlInexistente = new Querier("https://servicios.mercadolibre.kom.ar/");
		consulta = new Consulta("plomero","san miguel");
	}

	@Test
	public void generarUrlMlWebTest() {
		String url = "https://servicios.mercadolibre.com.ar/tecnico-en-computacion-lanus#D[A:tecnico%20en%20computacion%20lanus]";
		assertEquals(url, querierMl.generarUrlMlWeb("tecnico en computacion", "lanus"));
	}
	
	@Test
	public void querierExitosoTest() {
		assertNotNull(querierMl.find(consulta));
	}
	
	@Test
	public void querierFalloTest() {
		assertNull(querierMlUrlInexistente.find(consulta));
	}
	
	@Test
	public void notEqualsTest() {
		assertFalse(querierMl.equals(querierMlUrlInexistente));
	}
	
	@Test
	public void equalsTest() {
		assertTrue(querierMl.equals(new Querier(urlMl)));
	}
	
}
