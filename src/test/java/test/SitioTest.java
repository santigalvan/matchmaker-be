package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import builder.Config;
import builder.MercadoLibreApiBuilder;
import builder.MercadoLibreWebBuilder;
import builder.SitioInstanciator;
import deserialize.Deserialize;
import mercadolibre.MercadoLibre;
import querier.Querier;
import scrapper.Scrapper;
import sitio.Sitio;

public class SitioTest {
	
	Config config;
	String path;
	String file;
	Sitio mlWeb;
	Sitio mlApi;
	
	@Before
	public void setUp() throws Exception {
		path = System.getProperty("user.dir")+"/resources";
		file = "/sitios.txt";
		config = new Config(path+file);
		
		mlWeb = new MercadoLibreWebBuilder().build();
		mlApi = new MercadoLibreApiBuilder().build();
	}

	@Test
	public void instanciarSitiosTest() throws Exception {
		List<Sitio> sitios = new ArrayList<Sitio>();
		sitios.add(mlWeb);
		sitios.add(mlApi);
		
		assertArrayEquals(sitios.toArray(), SitioInstanciator.instanciar(config).toArray());
	}
	
	
	@Test
	public void builderMlWebTest() throws Exception {
		Sitio st = new Querier("https://servicios.mercadolibre.com.ar");
		st = new Scrapper(st);
		st = new Deserialize(st);
		
		assertEquals(st, mlWeb);
	}
	
	@Test
	public void builderMlApiTest() throws Exception {
		Sitio st = new MercadoLibre();
		st = new Deserialize(st);
		
		assertEquals(st, mlApi);
	}
	
		
}
