package test;

import static org.mockito.Mockito.*;

import java.io.IOException;
import java.lang.reflect.Field;

import org.junit.Before;
import org.junit.Test;

import io.swagger.client.ApiException;
import io.swagger.client.api.DefaultApi;
import junit.framework.Assert;
import mercadolibre.ConexionMercadoLibre;

public class ConexionMercadoLibreTest {

	String plomerosSanMiguel;
	String config;
	ConexionMercadoLibre conexion;
	
	@Before
	public void setUp() throws Exception {
		plomerosSanMiguel = "sites/MLA/search?q=plomero%20san%20miguel";
		config = System.getProperty("user.dir") + "/resources/ml.properties";
		
		conexion = ConexionMercadoLibre.getConexion(config);
		
		Class<?> clazz = ConexionMercadoLibre.class;
	   
	    Field field = clazz.getDeclaredField("instance");
        field.setAccessible(true);
        field.set(conexion, null);
        /*
        field = clazz.getDeclaredField("config");
        field.setAccessible(true);
        field.set(conexion, null);
		*/
        
        field = clazz.getDeclaredField("clientId");
        field.setAccessible(true);
        field.set(conexion, null);
        
        field = clazz.getDeclaredField("secretKey");
        field.setAccessible(true);
        field.set(conexion, null);
        
        field = clazz.getDeclaredField("api");
        field.setAccessible(true);
        field.set(conexion, null);
        
        	
	}

	@Test
	public void happyPathTest() throws ApiException, IOException {
		DefaultApi apiMock = mock(DefaultApi.class);
		when(apiMock.defaultGet(any())).thenReturn("Soy la api");
		
		ConexionMercadoLibre conexion = ConexionMercadoLibre.getConexion(config);
		conexion.setConexion(apiMock);
		
		Assert.assertEquals("Soy la api", conexion.get(plomerosSanMiguel));
	}
	
	@Test
	public void configErroneoTest() throws IOException {
		Assert.assertNull(ConexionMercadoLibre.getConexion("/home/algo/prop.properties"));
	}
	
	@Test
	public void getPlomerosSanMiguelTest() throws IOException {
		ConexionMercadoLibre conexion = ConexionMercadoLibre.getConexion(config);
		Assert.assertNotNull(conexion.get(plomerosSanMiguel));
	}
	
	

}
