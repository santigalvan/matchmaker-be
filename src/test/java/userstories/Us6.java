package userstories;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import circuitbreaker.CircuitBreaker;
import io.swagger.client.ApiException;
import io.swagger.client.api.DefaultApi;
import mercadolibre.Conexion;
import mercadolibre.ConexionMercadoLibre;
import mercadolibre.MercadoLibre;
import model.Consulta;

public class Us6 {

	MercadoLibre ml;
	ConexionMercadoLibre conexion;
	CircuitBreaker<String, Object> cbConexionMlMock;
	DefaultApi apiMock;
	Consulta gasistaSanMiguel;
	
	public static class ConexionFake implements Conexion {

		@Override
		public Object get(String resource) {
			return "Pedro";
		}
		
	}
	
	@SuppressWarnings("unchecked")
	@Before
	public void setUp() throws Exception {
		gasistaSanMiguel = new Consulta("Gasista", "San Miguel");
		conexion = ConexionMercadoLibre.getConexion(System.getProperty("user.dir") + "/resources/ml.properties");
		ml = new MercadoLibre();
		
		cbConexionMlMock = mock(CircuitBreaker.class);
		when(cbConexionMlMock.run()).thenReturn("El servicio no esta disponible");
		
		conexion.setCircuitBreaker(cbConexionMlMock);
		ml.setConexion(conexion);
		
	}
	
	@Test
	public void ca1() {
		assertEquals("El servicio no esta disponible" , ml.find(gasistaSanMiguel).toString());
	}
	
	@Test
	public void ca2() {
		ml.setConexion(new ConexionFake());
		assertEquals("Pedro" ,ml.find(gasistaSanMiguel).toString());
	}
	
	@Test
	public void ca3() throws ApiException, InterruptedException {
		conexion.setCircuitBreaker(new CircuitBreaker<String, Object>(4,4000));
		DefaultApi api = conexion.getApi(); //Guardo el llamado remoto real
		//Simulo que el servicio falla
		apiMock = mock(DefaultApi.class);
		when(apiMock.defaultGet(any())).thenThrow(new ApiException("Servicio no disponible"));
		
		ml.find(gasistaSanMiguel);
		ml.find(gasistaSanMiguel);
		ml.find(gasistaSanMiguel);
		ml.find(gasistaSanMiguel);
		ml.find(gasistaSanMiguel);
		//El breaker se rompe y pasa a OPEN
		Thread.sleep(5*1000);
		//Vuelve el servicio
		conexion.setDefaultApi(api);
		assertTrue(ml.find(gasistaSanMiguel).toString() != "El servicio no esta disponible");
	}
	

}
