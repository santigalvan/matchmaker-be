package userstories;

import static org.mockito.Mockito.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import deserialize.Deserialize;
import mercadolibre.MercadoLibre;
import model.Consulta;
import model.RespuestaDeserialize;
import model.RespuestaMercadoLibre;
import querier.Querier;
import sitio.Sitio;

public class Us3 {
	
	Sitio mlApi;
	Sitio mlApiFindEmpty;
	Consulta plomeriaSanMiguel;
	Consulta paleontologoNogues;
	
	String mlRegexInvalidaPath = System.getProperty("user.dir")+"/resources/sitio.MercadoLibreTest.properties";
	
	@Before
	public void setUp() throws Exception {
		plomeriaSanMiguel= new Consulta("plomeria","san miguel");
		paleontologoNogues= new Consulta("paleontologo", "nogues");
		
		MercadoLibre mlApiMock = mock(MercadoLibre.class);
		when(mlApiMock.find(any())).thenReturn(new RespuestaMercadoLibre("{title=plomeria general permiso habilitado por cuarentena, otra cosa=algo}"));
		mlApi = new Deserialize(mlApiMock);
		
		MercadoLibre mlApiMockEmpty = mock(MercadoLibre.class);
		when(mlApiMockEmpty.find(any())).thenReturn(new RespuestaMercadoLibre(""));
		mlApiFindEmpty = new Deserialize(mlApiMockEmpty);
	}

	@Test
	public void ca1() {
		JsonObject expected = JsonParser.parseString("{\"result\"=\"title=plomeria general permiso habilitado por cuarentena\"}")
				.getAsJsonObject();
		JsonArray actual = ((RespuestaDeserialize)mlApi.find(plomeriaSanMiguel)).getRespuesta();
	
		Assert.assertEquals(1, actual.size());
		Assert.assertEquals(expected, actual.get(0));
	}
	
	@Test
	public void ca2() {
		JsonArray expected = new JsonArray();
		JsonArray actual = ((RespuestaDeserialize)mlApiFindEmpty.find(paleontologoNogues)).getRespuesta();
	
		Assert.assertEquals(0, actual.size());
		Assert.assertEquals(expected, actual);
		
	}
	
	@Test(expected = Exception.class)
	public void ca3() throws Exception {
		Deserialize mlDeserialize = (Deserialize) mlApi;
		mlDeserialize.fillMap(mlRegexInvalidaPath);
	}
	
	@Test(expected = Exception.class)
	public void ca4() throws Exception {
		Sitio sitioNoExistente = new Querier("linkedin.com");
		sitioNoExistente = new Deserialize(sitioNoExistente);
	}
	
	

}
