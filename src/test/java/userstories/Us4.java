package userstories;
/*
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import builder.MercadoLibreApiBuilder;

import sitio.ConexionMercadoLibre;
import sitio.Sitio;
/*
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
*/
//@RunWith(PowerMockRunner.class)
//@PrepareForTest(ConexionMercadoLibre.class)

import org.junit.Before;
import org.junit.Test;

import io.swagger.client.ApiClient;
import io.swagger.client.ApiException;
import io.swagger.client.api.DefaultApi;
import junit.framework.Assert;
import mercadolibre.Conexion;
import mercadolibre.ConexionMercadoLibre;
import mercadolibre.MercadoLibre;
import model.Consulta;
import model.RespuestaMercadoLibre;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

public class Us4{

	MercadoLibre mlApi;
	ConexionMercadoLibre conexion;
	Consulta gasistaMoreno;
	Consulta albanilMoreno;
	
	public static class ConexionFake implements Conexion {

		@Override
		public Object get(String resource) {
			return "Pedro";
		}
		
	}
	
	public static class ConexionFakeVacia implements Conexion {

		@Override
		public Object get(String resource) {
			return "";
		}
		
	}
	
	@Before
	public void setUp() throws Exception {
		gasistaMoreno = new Consulta("gasista", "moreno");
		albanilMoreno = new Consulta("albañil", "moreno");
		
		mlApi = new MercadoLibre();
		mlApi.setConexion(new ConexionFake());
	}
	
	@Test
	public void ca1() {
		assertTrue("Pedro".equals(((RespuestaMercadoLibre)mlApi.find(gasistaMoreno)).toString()));
	}
	
	@Test
	public void ca2() {
		mlApi.setConexion(new ConexionFakeVacia());
		assertTrue("".equals(((RespuestaMercadoLibre)mlApi.find(albanilMoreno)).toString()));
	}
	
	@Test
	public void ca3() throws IOException, ApiException {
		DefaultApi apiMock = mock(DefaultApi.class);
		when(apiMock.defaultGet(any())).thenThrow(new ApiException());
		
		ConexionMercadoLibre conexion = ConexionMercadoLibre.getConexion(System.getProperty("user.dir") + "/resources/ml.properties");
		conexion.setConexion(apiMock);
		
		Assert.assertEquals(null, conexion.get("/sites/tarjetasCredito/all"));
	}
	
	@Test
	public void ca4() {
		DefaultApi apiErronea = new DefaultApi(new ApiClient(),59565L,"usuari1");
		ConexionMercadoLibre conexion = ConexionMercadoLibre.getConexion(System.getProperty("user.dir") + "/resources/ml.properties");
		conexion.setConexion(apiErronea);
		conexion.get("sites/MLA/search?q=plomeria");
	}
	
	
	

}
