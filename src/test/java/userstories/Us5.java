package userstories;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import cache.CachePoliticaAleatoria;
import cache.CacheSitio;
import model.Consulta;
import model.RespuestaScrapper;
import scrapper.Scrapper;
import sitio.Sitio;

public class Us5 {

	Sitio scrapper;
	Consulta gasistaMoreno;
	CachePoliticaAleatoria cache;
	
	@Before
	public void setUp() throws Exception {
		gasistaMoreno = new Consulta("Gasista", "Moreno");
		cache = new CachePoliticaAleatoria(5);
		//cacheMock = mock(CachePoliticaRandom.class);
		//when(cacheMock.estaEnCache(any())).thenReturn(false);
		//when(cacheMock.almacenar(any(), any()));

		Scrapper deserializeMock = mock(Scrapper.class);
		when(deserializeMock.find(any())).thenReturn(new RespuestaScrapper("Pedro"));
		scrapper = new CacheSitio(deserializeMock, cache);
	}

	@Test
	public void ca1() {
		Integer sizeActual = cache.cantRegistros();
		String find = ((RespuestaScrapper)scrapper.find(gasistaMoreno)).toString();
		
		assertEquals("Pedro", find);
		assertEquals((Integer) (sizeActual+1), cache.cantRegistros());
	}
	
	@Test
	public void ca2() {
		scrapper.find(gasistaMoreno);
		Integer sizeActual = cache.cantRegistros();
		String find = ((RespuestaScrapper)scrapper.find(gasistaMoreno)).toString();
		
		assertEquals("Pedro", find);
		assertEquals(sizeActual, cache.cantRegistros());
	}
	
	@Test
	public void ca3() {
		cache.almacenar(new Consulta("Plomero","San Miguel"), new RespuestaScrapper("Pedro"));
		cache.almacenar(new Consulta("Gasista","San Miguel"), new RespuestaScrapper("Pedro"));
		cache.almacenar(new Consulta("Musico","San Miguel"), new RespuestaScrapper("Pedro"));
		cache.almacenar(new Consulta("Carpintero","San Miguel"), new RespuestaScrapper("Pedro"));
		cache.almacenar(new Consulta("Programador","San Miguel"), new RespuestaScrapper("Pedro"));
		
		cache.almacenar(new Consulta("Fumigador","San Miguel"), new RespuestaScrapper("Pedro"));
		assertTrue(cache.cantRegistros() < 5);
		assertTrue(cache.estaEnCache(new Consulta("Fumigador","San Miguel")));
		
	}
	
}
