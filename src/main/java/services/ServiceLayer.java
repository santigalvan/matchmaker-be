package services;

import java.util.List;

import dto.ConsultaDTO;
import dto.DTOMapper;
import dto.RespuestaDTO;
import model.Consulta;
import model.Respuesta;

public class ServiceLayer {
	
	private SitioService sitioService = SitioService.getInstance(); 
	
	public List<RespuestaDTO> find(ConsultaDTO consultaDTO){
		Consulta consulta = DTOMapper.dissambler(consultaDTO);
		List<Respuesta> respuesta = sitioService.find(consulta);
		System.out.println(respuesta);
		List<RespuestaDTO> ret = DTOMapper.dissambler(respuesta);
		return ret;
	}

}
