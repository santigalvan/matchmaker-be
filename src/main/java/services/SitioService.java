package services;

import java.util.List;

import builder.Config;
import builder.SitioInstanciator;
import model.Consulta;
import model.Respuesta;
import sitio.Orquestador;
import sitio.Sitio;

public class SitioService {
	
	private Orquestador orq;
	private static SitioService instance;

	public List<Respuesta> find(Consulta consulta) {
		return orq.find(consulta);
	}
	
	private void inicializar(){
		try{
			String path = System.getProperty("user.dir")+"/resources";
			String file = "/sitios.txt";
			Config config = new Config(path+file);
			List<Sitio> sitios = SitioInstanciator.instanciar(config);
			orq = new Orquestador(sitios);
		}catch(Exception e) {
			
		}
		
	}
	
	public static SitioService getInstance(){
		if(instance == null) {
			instance = new SitioService();
			instance.inicializar();
			return instance;
		}
		else {
			return instance;
		}
	}
	
	

}
