package sitio;

import model.Consulta;
import model.Respuesta;

public interface Sitio {
	
	public Respuesta find(Consulta consulta);
}
