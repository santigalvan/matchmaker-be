package sitio;

public abstract class SitioDecorator implements Sitio {

	protected Sitio sitio;
	
	public SitioDecorator(Sitio sitio) {
		setSitio(sitio);
	}
		
	public SitioDecorator() {}

	
	public void setSitio(Sitio sitio) {
		this.sitio = sitio;
	}
	
	
	

}
