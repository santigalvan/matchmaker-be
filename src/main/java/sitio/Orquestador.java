package sitio;

import java.util.ArrayList;
import java.util.List;

import model.Consulta;
import model.Respuesta;

public class Orquestador {
	
	private List<Sitio> sitios;
	 
	public Orquestador(List<Sitio> sitios) {
		this.sitios = sitios;
	}
	
	public List<Respuesta> find(Consulta consulta) {
		List<Respuesta> result = new ArrayList<Respuesta>();
		Respuesta response = null;
		for(Sitio sitio: sitios) {	
			response = sitio.find(consulta);
			result.add(response);
		}
		return result;
	}
	 
}
