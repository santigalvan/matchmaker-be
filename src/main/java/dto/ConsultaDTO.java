package dto;

public class ConsultaDTO {
	private String servicio;
	private String zona;
	
	public ConsultaDTO(String servicio, String zona) {
		super();
		this.servicio = servicio;
		this.zona = zona;
	}
	public String getServicio() {
		return servicio;
	}

	public String getZona() {
		return zona;
	}

	
	

}
