package dto;

public class RespuestaDTO {
	private String descripcion;
	
	public RespuestaDTO(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
