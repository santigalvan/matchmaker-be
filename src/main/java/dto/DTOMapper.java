package dto;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import model.Consulta;
import model.Respuesta;
import model.RespuestaDeserialize;

public class DTOMapper {
	
	public static Consulta dissambler(ConsultaDTO consultaDTO) {
		Consulta consulta = new Consulta(consultaDTO.getServicio(), consultaDTO.getZona());
		return consulta;
	}
	
	public static List<RespuestaDTO> dissambler(List<Respuesta> respuesta) {
		List<RespuestaDTO> respuestas = new ArrayList<RespuestaDTO>();
		
		for(Respuesta resp: respuesta) {
			try {
				JsonArray res = ((RespuestaDeserialize) resp).getRespuesta();
				String descripcion;
				for(JsonElement elemnt : res.getAsJsonArray()) {
					descripcion = (String) elemnt.toString().subSequence(11, elemnt.toString().length()-1);
					respuestas.add(new RespuestaDTO(descripcion));
				}
			}catch(Exception e) {
				respuestas.add(new RespuestaDTO(e.toString()));
			}
			
		}
		return respuestas;
	}
	
}
