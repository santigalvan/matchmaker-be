package cache;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import model.Consulta;
import model.Respuesta;

public class CachePoliticaAleatoria implements Cache<Consulta, Respuesta>{
	
	private Map<Consulta, Respuesta> cache;
	private Integer tamanioCache;
	
	public CachePoliticaAleatoria(Integer size) {
		this.cache = new HashMap<Consulta, Respuesta>();
		this.tamanioCache = size;
	}
	
	public void almacenar(Consulta consulta, Respuesta respuesta) {
		if(!hayLugar()) {
			politicaDeReemplazo();
		}
		this.cache.put(consulta, respuesta);
	}
	
	public boolean estaEnCache(Consulta consulta) {
		return cache.containsKey(consulta);
	}
	
	public Respuesta obtener(Consulta consulta) {
		return cache.get(consulta);
	}
	
	public void politicaDeReemplazo() {
		Iterator<Entry<Consulta, Respuesta>> it = cache.entrySet().iterator();
		while(it.hasNext()) {
			it.next();
			Integer eliminar = (int) Math.round(Math.random());
			if(eliminar==1) {
				it.remove();
			}
		}
	}
		
	public boolean hayLugar() {
		return cantRegistros()> tamanioCache;
	}
	
	public Integer cantRegistros(){
		return this.cache.keySet().size();
	}
	
	
}
