package cache;

public interface Cache<T,V> {
	
	public void almacenar(T key, V value);
	
	public V obtener(T key);
	
	public void politicaDeReemplazo();
	
	public boolean estaEnCache(T key);
	
	public boolean hayLugar();
	
	public Integer cantRegistros();
	
}
