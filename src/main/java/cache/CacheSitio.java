package cache;

import model.Consulta;
import model.Respuesta;
import sitio.Sitio;
import sitio.SitioDecorator;

public class CacheSitio extends SitioDecorator {
	
	private Cache<Consulta, Respuesta> cache;
	
	public CacheSitio(Sitio sitio, Cache<Consulta, Respuesta> cache) {
		super(sitio);
		this.cache = cache;
	}

	@Override
	public Respuesta find(Consulta consulta) {
		if(cache.estaEnCache(consulta))
			return cache.obtener(consulta);
		else {
			Respuesta respuesta = super.sitio.find(consulta);
			cache.almacenar(consulta, respuesta);
			return respuesta;
		}
	}
	
	

}
