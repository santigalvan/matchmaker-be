package deserialize;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import mercadolibre.MercadoLibre;
import model.Consulta;
import model.Respuesta;
import model.RespuestaDeserialize;
import scrapper.Scrapper;
import sitio.Sitio;
import sitio.SitioDecorator;


public class Deserialize extends SitioDecorator {
		
	private Map<String, String> findReplace;

	public Deserialize(Sitio sitio) throws Exception {
		super(sitio);
		//Muy, muy, muy horrible, pero no puedo testear
		String className = obtenerClassName(sitio.getClass().getName().toString());
		String path = System.getProperty("user.dir")+"/resources/"+className+".properties";
		try {
			fillMap(path);
		}catch(Exception e) {
			throw new Exception("No existe una configuracion del sitio");
		}
	}

	public void fillMap(String path) throws Exception {
		findReplace = new HashMap<String, String>();
		Properties prop = new Properties();
		InputStream is;
		try {
			is = new FileInputStream(path);
			prop.load(is);
			
			for(Enumeration<Object> e = prop.keys(); e.hasMoreElements();) {
				System.out.println();
				Object obj = e.nextElement();
				String[] keyValue = prop.getProperty(obj.toString()).split("@");
				findReplace.put(keyValue[0], keyValue[1]);
			}
		}catch(IOException e) {
			throw new IOException("No se encontro el path");
		}catch(Exception e) {
			throw new Exception("Regex invalida");
		}
	}

	@Override
	public Respuesta find(Consulta consulta) {
		Respuesta find = sitio.find(consulta);
		String toDeserialize = find.toString();
		JsonArray ret= new JsonArray();
		for (String regex: findReplace.keySet()) {
			Pattern patt = Pattern.compile(regex);
			Matcher matcher = patt.matcher(toDeserialize);
			String jsonTemplate = findReplace.get(regex);
			String value;
			JsonObject json;
			
			while(matcher.find()) {
				value = jsonTemplate.replaceAll("#", matcher.group(0));
				json = JsonParser.parseString(value).getAsJsonObject();
				ret.add(json);
			}
		}
		RespuestaDeserialize respuesta = new RespuestaDeserialize(ret);
		return respuesta;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o==null) return false;
		if(!(o instanceof Deserialize)) return false;
		Deserialize other = (Deserialize) o;
		
		return other.sitio instanceof Scrapper || other.sitio instanceof MercadoLibre;
	}
	
	//Por favor, no miren este metodo.
	private String obtenerClassName(String className) {
		String ret="";
		int i = 0;
		while(i < className.length() && !(className.charAt(i) == '$')) {
			ret = ret + className.charAt(i);
			i++;
		}
		return ret;
	}
	
}


