package mercadolibre;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.function.Function;

import circuitbreaker.CircuitBreaker;
import io.swagger.client.ApiClient;
import io.swagger.client.ApiException;
import io.swagger.client.api.DefaultApi;

public class ConexionMercadoLibre implements Conexion {
	
	private static ConexionMercadoLibre instance;
	private Long clientId;  
    private String secretKey;
	private DefaultApi api;
	private CircuitBreaker<String, Object> cb;
	
	
	private ConexionMercadoLibre(String config) throws IOException {
		Properties prop = new Properties();
		InputStream is = null;
		this.cb = new CircuitBreaker<>(4, 4000);
		
		try {
			is = new FileInputStream(config);
			prop.load(is);
		} catch(IOException e) {
			throw new IOException("No existe el path");
		}

		secretKey = prop.getProperty("secretKey");
		clientId = Long.valueOf(prop.getProperty("clientId"));
		api = new DefaultApi(new ApiClient(), clientId, secretKey);
	}
	
	public static ConexionMercadoLibre  getConexion(String config) {
		if(instance == null) {
			try {
				instance = new ConexionMercadoLibre(config);
			} catch (IOException e) {
				System.out.println("No se pudo establecer la conexion");
				return null;
			}
		}
		return instance;
	}
	
	public void setConexion(DefaultApi api) {
		this.api = api;
	}
	
	public void setCircuitBreaker(CircuitBreaker<String, Object> cb) {
		this.cb = cb;
	}
	
	public void setDefaultApi(DefaultApi api) {
		this.api = api;
	}
	
	public DefaultApi getApi() {
		return this.api;
	}
	
	public Object get(String resource) {
		try {
			Function<String, Object> fc = (String url) -> wrapGet(api, resource);
			cb.setFunc(fc);
			cb.setParams(resource);
			
			return cb.run();
		} catch (Exception e) {
			return "El servicio no se encuentra disponible";
		}
	}
	
	private Object wrapGet(DefaultApi api ,String url){
		try {
			return api.defaultGet(url);
		}catch(ApiException e) {
			e.getStackTrace();
			return null;
		}
	}
	
	/*
	public void init(String config) throws IOException {
		if (this.config == null) {
			this.config = config;
            try {
				instance = new ConexionMercadoLibre(this.config);
			} catch (IOException e) {
				throw new IOException("No existe el path");
			}
		}
        else
            throw new IllegalStateException("La conexion ya ha sido inicializada");
	}
	*/
	
	/*
	private String getAuthUrl() {
		try {
			return api.getAuthUrl(redirectUri, Configuration.AuthUrls.MLA);
		} catch (ApiException e) {
			e.printStackTrace();
			return "";
		} 
	}
	
	private void authorize() {
		 DefaultApi api = new DefaultApi(new ApiClient(), clientId, secretKey);
	     String code = "TG-5eb71a6a0f46b500065b9116-502057569";
	     AccessToken response;
	     try {
			response = api.authorize(code, redirectUri);
			
	     }catch (ApiException e) {
			e.printStackTrace();
	     }
	}
	
	*/
}
