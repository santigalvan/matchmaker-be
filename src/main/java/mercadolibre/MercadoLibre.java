package mercadolibre;

import model.Consulta;
import model.Respuesta;
import model.RespuestaMercadoLibre;
import sitio.Sitio;

public class MercadoLibre implements Sitio{
	
	private Conexion conexionMercadoLibre = ConexionMercadoLibre.getConexion(System.getProperty("user.dir") + "/resources/ml.properties");
	
	@Override
	public Respuesta find(Consulta consulta) {
		
		Object response = conexionMercadoLibre.get("sites/MLA/search?q="+consulta.getServicio()+"%20"+consulta.getZona());
		
		Respuesta respuesta = new RespuestaMercadoLibre(response);
		return respuesta;
	}
	
	public void setConexion(Conexion conexion) {
		conexionMercadoLibre = conexion;
	}
	
}
