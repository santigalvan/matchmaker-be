package scrapper;

import java.util.List;

import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import model.Consulta;
import model.Respuesta;
import model.RespuestaQuerier;
import model.RespuestaScrapper;
import querier.Querier;
import sitio.Sitio;
import sitio.SitioDecorator;

public class Scrapper extends SitioDecorator {
	
	public Scrapper(Sitio sitio) {
		super(sitio);
	}

	@Override
	public Respuesta find(Consulta consulta) {	
		RespuestaQuerier respuestaQuerier = (RespuestaQuerier) sitio.find(consulta);
		HtmlPage page = respuestaQuerier.getPage();
		List<DomElement> spans = page.getElementsByTagName("span");
		StringBuilder st = new StringBuilder();
		
		Respuesta respuesta;
		for (DomElement element : spans) {
		    if(element.getAttribute("class").equals("main-title")) {
		    	st.append(element.asText());
		    	st.append(":");
		    } 
		    if(element.getAttribute("class").equals("price__text")) {
		    	st.append(element.asText());
		    	st.append(";");
		    }
		}
		respuesta = new RespuestaScrapper(st.toString());
		return  respuesta;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o==null) return false;
		if(!(o instanceof Scrapper)) return false;
		Scrapper other = (Scrapper) o;
		
		return other.sitio instanceof Querier;
	}
	
}
