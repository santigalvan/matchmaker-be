package querier;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.function.Function;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import circuitbreaker.CircuitBreaker;
import model.Consulta;
import model.Respuesta;
import model.RespuestaQuerier;
import sitio.SitioDecorator;

public class Querier extends SitioDecorator {
	
	private String url;
	private CircuitBreaker<String, HtmlPage> cb;
	
	public Querier(String url) {
		super();
		this.url = url;
		super.setSitio(this);
		this.cb = new CircuitBreaker<>(5, 4000);
	}
	
	@Override
	public Respuesta find(Consulta consulta) {		
		WebClient cliente = new WebClient();
		cliente.getOptions().setJavaScriptEnabled(false);
		cliente.getOptions().setCssEnabled(false);
		cliente.getOptions().setUseInsecureSSL(true);
			
		HtmlPage page;
		Respuesta respuesta;
		
		try {
			Function<String, HtmlPage> fc = (String url) -> wrapGetPage(cliente, url);
			cb.setFunc(fc);
			cb.setParams(generarUrlMlWeb(consulta.getServicio(), consulta.getZona()));
			page = cb.run();
			respuesta = new RespuestaQuerier(page);
			
			return respuesta;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} 
	}
	
	private HtmlPage wrapGetPage(WebClient cliente ,String url) {
		try {
			return cliente.getPage(url);
		}catch(IOException e ) {
			throw new UncheckedIOException(e);
		}
	}
	
	public String generarUrlMlWeb(String servicio, String zona) {
		String primeraParte = (servicio+" "+zona).replace(" ", "-");
		String segundaParte = (servicio+" "+zona).replace(" ", "%20");
		
		return this.url+"/"+primeraParte+"#D[A:"+segundaParte+"]";
	}
	
	public void setCircuitBreaker(final CircuitBreaker<String, HtmlPage> cb) {
		this.cb = cb;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o==null) return false;
		if(!(o instanceof Querier)) return false;
		Querier other = (Querier) o;
		return this.url.equals(other.url);
	}

}
