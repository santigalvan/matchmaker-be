package builder;

import java.util.ArrayList;
import java.util.List;

import sitio.Sitio;

public class SitioInstanciator {
	
	@SuppressWarnings("deprecation")
	public static List<Sitio> instanciar(Config cfg) throws Exception{
		List<Sitio> ret = new ArrayList<>();
		List<String> entries = cfg.getEntries();
		
		SitioBuilder builder;
		for(String e: entries){
			Class<?> c = Class.forName(e);
			builder = (SitioBuilder)c.newInstance() ;
			if(!(builder instanceof SitioBuilder)){
				return null;
			}
			ret.add(builder.build());
		}
		return ret;
	}
}
