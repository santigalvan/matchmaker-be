package builder;

import java.io.BufferedReader;
import java.io.File;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Config{
    
    private List<String> entries;

    public Config(String ruta) throws IOException{
        entries = new ArrayList<String>();
        
        FileReader archivo = new FileReader(new File(ruta));
        BufferedReader buffer = new BufferedReader(archivo);
        
        String linea;
        while((linea=buffer.readLine())!= null){
            entries.add(linea);
        }
        buffer.close();
    }

    public List<String> getEntries(){
        return entries;
    }


}
