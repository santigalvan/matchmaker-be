package builder;


import deserialize.Deserialize;
import querier.Querier;
import scrapper.Scrapper;
import sitio.Sitio;

public class MercadoLibreWebBuilder implements SitioBuilder {

	@Override
	public Sitio build() throws Exception {
		Sitio ml;
		ml = new Querier("https://servicios.mercadolibre.com.ar");
		ml = new Scrapper(ml);
		ml = new Deserialize(ml);
		//ml = new CacheSitio(ml, new CachePoliticaAleatoria(5));
		
		return ml;
	}
	
}
