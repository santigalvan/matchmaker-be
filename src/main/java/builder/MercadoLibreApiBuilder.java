package builder;

import deserialize.Deserialize;
import mercadolibre.MercadoLibre;
import sitio.Sitio;

public class MercadoLibreApiBuilder implements SitioBuilder {

	@Override
	public Sitio build() throws Exception {
		Sitio ml = new MercadoLibre();
		ml = new Deserialize(ml);
		//ml = new CacheSitio(ml, new CachePoliticaAleatoria(5));
		return ml;
	}
	
	
	
}
