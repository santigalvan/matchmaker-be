package builder;

import sitio.Sitio;

public interface SitioBuilder {
	
	public Sitio build() throws Exception;

}
