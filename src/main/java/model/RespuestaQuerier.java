package model;

import com.gargoylesoftware.htmlunit.html.HtmlPage;

public class RespuestaQuerier implements Respuesta{
	
	private HtmlPage page;
	
	public RespuestaQuerier(HtmlPage page) {
		this.page = page;
	}
	
	public HtmlPage getPage() {
		return this.page;
	}
	
}
