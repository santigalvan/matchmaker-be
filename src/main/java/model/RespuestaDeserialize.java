package model;

import com.google.gson.JsonArray;

public class RespuestaDeserialize implements Respuesta {

	private JsonArray respuesta;
	
	public RespuestaDeserialize(JsonArray ret) {
		this.respuesta = ret;
	}
	
	public JsonArray getRespuesta() {
		return respuesta;
	}

}
