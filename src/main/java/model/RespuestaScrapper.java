package model;

public class RespuestaScrapper implements Respuesta {
	
	private String respuesta;
	
	public RespuestaScrapper(String respuesta) {
		this.respuesta = respuesta;
	}
	
	@Override
	public String toString() {
		return respuesta.toString();
	}
	
}

