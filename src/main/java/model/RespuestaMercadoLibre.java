package model;

public class RespuestaMercadoLibre implements Respuesta{
	
	private String respuesta;
	

	public RespuestaMercadoLibre(Object response) {
		this.respuesta = response.toString();
	}
	
	@Override
	public String toString() {
		return respuesta.toString();
	}

}
