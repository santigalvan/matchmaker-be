package circuitbreaker;

import java.sql.Timestamp;
import java.util.function.Function;

import org.apache.commons.net.ntp.TimeStamp;

public class CircuitBreaker<T,V> {
	private State state;
	private Integer intentosFallidos;
	private Integer threshold;
	private TimeStamp ultimafalla;
	private Function<T,V> func;
	private T params;
	private long tiemporeintento;
	
	public CircuitBreaker(Integer threshold, long tiempointento) {
		this.threshold = threshold;
		this.tiemporeintento = tiempointento;
		this.state = State.CLOSED;
		this.intentosFallidos = 0;
	}

	public V run() throws Exception {
		update();
		switch(state) {
			case CLOSED:
			case HALF_OPEN:
				try {
					V response = func.apply(params);
					reset();
					return response;
				}catch(Exception e) {
					guardarFalla();
				}
			case OPEN:
				throw new Exception("El servicio no esta disponible");
			default:
				throw new Exception("No se pudo reconocer el estado");
		}
	}
	
	private void update() {
		if (this.intentosFallidos > this.threshold) {
			
			if (((new Timestamp(System.currentTimeMillis())).getTime() - this.ultimafalla.getTime() ) > tiemporeintento) {
				this.state = State.HALF_OPEN;
			}
			else {
				this.state = State.OPEN;
			}
		}
		else {
			this.state = State.CLOSED;
		}
	}
	
	private void reset() {
		this.intentosFallidos = 0;
		this.ultimafalla = null;
		this.state = State.CLOSED;
	}
	
	private void guardarFalla() {
		this.ultimafalla = TimeStamp.getCurrentTime();
		this.intentosFallidos++;	
	}
	
	public void setFunc(Function<T, V> func) {
		this.func = func;
	}

	public void setParams(T params) {
		this.params = params;
	}
	
}
